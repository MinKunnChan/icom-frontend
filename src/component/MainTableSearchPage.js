import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useInput } from '../hooks/input';

const channels = [
    {
      value: '',
      label: '',
    },
    {
      value: 'SMS',
      label: 'SMS',
    },
    {
      value: 'EMAIL',
      label: 'EMAIL',
    }
  ];

  
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch',
    },
	},
	header: {
		marginLeft: 6
	},
	button: {
    '& > *': {
      margin: theme.spacing(.5),
    },
  }
}));


export default function MainTableSearchPage(props) {

    const classes = useStyles();

		const { value:keywordId, bind:bindKeywordId, reset:resetKeywordId } = useInput('');
		const { value:keyword, bind:bindKeyword, reset:resetKeyword } = useInput('');
		const { value:emailAddress, bind:bindEmail, reset:resetEmail} = useInput('');
		const { value:billingAccount, bind:bindBillingAccount, reset:resetBillingAccount } = useInput('');
		const { value:regularExpression, bind:bindRegularExpression, reset:resetRegularExpression } = useInput('');
		const { value:autoResponseTemplateId, bind:bindAutoResponseTemplateId, reset:resetAutoResponseTemplateId } = useInput('');
		const { value:channel, bind:bindChannel, reset:resetChannel } = useInput('');

		const handleSubmit = (evt) => {
			evt.preventDefault();
			const searchData = {
				keywordId, keyword, emailAddress, billingAccount, regularExpression,
				autoResponseTemplateId, channel
			}
      props.onSearch(searchData)
		}
		
		const resetForm = () =>{
			resetKeywordId();
			resetKeyword();
			resetEmail();
			resetBillingAccount();
			resetRegularExpression();
			resetAutoResponseTemplateId();
			resetChannel();
		}

    return (
        <Paper variant="outlined">
					<Typography className={classes.header} variant="h6" gutterBottom>
						Search
					</Typography>
          <form className={classes.root} onSubmit={handleSubmit} noValidate autoComplete="off">
						<TextField id="keywordId" {...bindKeywordId} label="Keyword Id" fullWidth
							size="small" margin="normal" variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
						<TextField id="keyword" label="Keyword" fullWidth
							size="small" margin="normal" {...bindKeyword} variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
						<TextField id="emailAddress" {...bindEmail} label="Email" fullWidth
							size="small" type="email" margin="normal" variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
						<TextField id="billingAccount" {...bindBillingAccount} label="Billing Account" fullWidth
							size="small" margin="normal" variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
						<TextField id="regularExpression" {...bindRegularExpression} label="Regular Expression" fullWidth
							size="small" margin="normal" variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
						<TextField id="autoResponseTemplateId" {...bindAutoResponseTemplateId} label="Auto Response TemplateId" fullWidth
							size="small" margin="normal" variant="outlined"
							InputLabelProps={{
								shrink: true,
							}}/>
            <TextField id="channel" select label="Channel" size="small"
                value={channel} {...bindChannel} variant="outlined"
                SelectProps={{
                  native: true,
								}}>
                { channels.map((option) => (
                    <option key={option.value} value={option.value}>
                    	{option.label}
                    </option>
                ))}
            </TextField>
						<div className={classes.button}>
							<Button variant="outlined" type="submit" color="primary">
								Search
							</Button>
							<Button onClick={resetForm} variant="outlined">
								Clear
							</Button>
						</div>
          </form>
        </Paper>
    );
}