import React, {useEffect, useState, useContext} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import { Link } from 'react-router-dom';
import { UserContext } from "../context/UserContext";
import * as Url from '../const/urls';

import MainTableSearchPage from "./MainTableSearchPage";

const columns = [
    { id: 'keywordId', label: 'Keyword ID', minWidth: 150 },
    { id: 'keyword', label: 'Keyword', minWidth: 80 },
    { id: 'emailAddress',label: 'Email', minWidth: 150, align: 'right',},
    { id: 'billingAccount', label: 'Billing Account', minWidth: 50, align: 'right', },
    { id: 'regularExpression', label: 'Regular Exp', minWidth: 100, align: 'right', },
    { id: 'channel',label: 'Channel', minWidth: 40},
    { id: 'channelSms', label: 'Channel Sms', minWidth: 20,align: 'right', format: (value) => value ? "Y":"N" },
    { id: 'channelEmailSubject', label: 'Channel Email Subject', minWidth: 20,align: 'right', format: (value) => value ? "Y":"N" },
    { id: 'channelEmailBody',label: 'Channel Email Body', minWidth: 20,align: 'right', format: (value) => value ? "Y":"N"},
    { id: 'autoResponseRequired', label: 'Auto Response Required', minWidth: 20,align: 'right', format: (value) => value ? "Y":"N" },
    { id: 'autoResponseTemplateId', label: 'Auto Response TemplateId', minWidth: 100, align: 'right', },
    { id: 'forwardToMessageFlag',label: 'Forward To Message', minWidth: 20, align: 'right', format: (value) => value ? "Y":"N"},
    { id: 'downloadBipFlag',label: 'Download Bip', minWidth: 20,align: 'right', format: (value) => value ? "Y":"N"},
  ];

  const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 450,
    },
    header: {
      marginTop: '80px',
    }
  });

  export default function MainTablePage(props) {
    const classes = useStyles();
    
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [totalElements, setTotalElements] = useState(10);
    const [rows, setRows] = useState([]);
    const url = Url.getMessages + `?page=${page}&size=${rowsPerPage}`;
    const [queryUrl, setQueryUrl] = useState('');

    const { user, setUser } = useContext(UserContext);

  
  useEffect(() => {
    fetchData(url + queryUrl);
  },[page, rowsPerPage, queryUrl, url]);

  const fetchData = (url) =>{
    fetch(url)
    .then(response => {
      if (!response.ok) {
        throw new Error('Failed to fetch.');
      }
      return response.json();
    })
    .then(data => {
      setRows(data.content);
      setPage(data.number);
      setRowsPerPage(data.size);
      setTotalElements(data.totalElements);
    })
    .catch(err => {
      console.log(err);
    });
  }

    const handleChangePage =(event, newPage) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

    const handleSearch = data => {
      const queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
      setQueryUrl("&"+ queryString);
    }

    const handleCellClick = (row) =>{
       props.history.push('/details/'+ row.keywordId)
    }
  
    return (
      <div className={classes.header}>
      <MainTableSearchPage onSearch={handleSearch}/>
      <button onClick={() => {
            const user = {
              name: "From Home Page", age: 20
            };
            setUser(user);
          }}>Set User</button>
      <button onClick={() => {
        setUser(null);
      }}>Set User</button>
      {JSON.stringify(user)} <Link to="/about">About</Link>
      <Paper variant="outlined" className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table" >
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => {
                return (
                  <TableRow onDoubleClick={() => handleCellClick(row)} 
                    hover role="checkbox" key={index}>
                    {columns.map((column, index) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={index} align={column.align}>
                          {column.format ? column.format(value) : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={totalElements}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
        
					<Link variant="outlined" color="primary" href="#outlined-buttons" style={{textDecoration: 'none'}} to="/form">Create New</Link>
      </Paper>
      </div> 
    );
  }