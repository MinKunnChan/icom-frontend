import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import * as Url from '../const/urls';

const useStyles = makeStyles((theme) => ({
  root:{
    marginTop: 80
  },
	header: {
		marginLeft: 6
	}
}));


export default function MaintableDetailPage(props) {

  const parmaId = props.match.params.id;
  const classes = useStyles();

  const [fetchedData, setFetchedData] = useState({});
  const url = Url.getMessage + `${parmaId}`;

  useEffect(() => {
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch.');
        }
        return response.json();
      })
      .then(data => {
        setFetchedData(data);
      })
      .catch(err => {
        console.log(err);
      });
  }, [ url]);

  return (
      <Paper className={classes.root} variant="outlined">
        <Typography className={classes.header} variant="h6" gutterBottom>
          Details
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <ListItemText primary="Keyword Id" secondary={fetchedData.keywordId}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Keyword" secondary={fetchedData.keyword}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Email Address" secondary={fetchedData.emailAddress} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Billing Account" secondary={fetchedData.billingAccount}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Regular Expression" secondary={fetchedData.regularExpression}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Channel" secondary={fetchedData.channel} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Channel Sms" secondary={fetchedData.channelSms ? "Y" : "N"}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Channel Email Subject" secondary={fetchedData.channelEmailSubject ? "Y" : "N"}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Channel Email Body" secondary={fetchedData.channelEmailBody  ? "Y" : "N"}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Auto Response Required" secondary={fetchedData.autoResponseRequired  ? "Y" : "N"}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Auto Response TemplateId" secondary={fetchedData.autoResponseTemplateId}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="ForwardTo Message" secondary={fetchedData.forwardToMessageFlag  ? "Y" : "N"}/>
              </ListItem>
              <ListItem>
                <ListItemText primary="Download Bip" secondary={fetchedData.downloadBipFlag  ? "Y" : "N"}/>
              </ListItem>
            </List>
        </Typography>
      </Paper>
  );
}