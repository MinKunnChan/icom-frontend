import React, { useContext } from "react";
import { UserContext } from "../context/UserContext";
import { Link } from "react-router-dom";

export function AboutPage() {
  const { user, setUser } = useContext(UserContext);

  return (
    <div>
      <h2>About</h2> <Link to="/">Home</Link>
      <pre>{JSON.stringify(user)}</pre>

      <button onClick={() => {
            const user = {
              name: "From About Page", age: 20
            };
            setUser(user);
          }}>Set User</button>

    </div>
  );
}