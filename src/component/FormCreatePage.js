import React, {useState, useEffect} from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Kafka } from 'kafkajs';

const currencies = [
    {
      value: 'USD',
      label: '$',
    },
    {
      value: 'EUR',
      label: '€',
    },
    {
      value: 'BTC',
      label: '฿',
    },
    {
      value: 'JPY',
      label: '¥',
    },
  ];

  
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  top:{
    marginTop: 80
  },
	header: {
		marginLeft: 6
	},
	button: {
    '& > *': {
      margin: theme.spacing(.5),
    },
  }
}));


export default function FormCreatePage() {

    const classes = useStyles();

    const [currency, setCurrency] = useState('EUR');
    const [users, setUsers] = useState([]);

    useEffect(()=>{

    }, [users]);

    

    const runKafka = async () =>{
      const kafka = new Kafka({
        clientId: 'producer-1',
        brokers: ['localhost:9092']
      });
      const consumer = kafka.consumer({ groupId: 'group_id' })
      try{
        await consumer.connect();
        await consumer.subscribe({ topic: 'Users', fromBeginning: true })
    
        await consumer.run({
          eachMessage: async ({ topic, partition, message }) => {
            console.log({
              value: message.value.toString(),
            })
            users.push(message.value.toString());
            setUsers(users);
          },
        })
      }catch (ex){
        console.error(ex)
      }
      
    }

    runKafka();

    const handleChange = (event) => {
        setCurrency(event.target.value);
    };

    return (
        <Paper className={classes.top} variant="outlined">
					<Typography className={classes.header} variant="h6" gutterBottom>
						Form Create Page
					</Typography>
          <form className={classes.root} noValidate autoComplete="off">
						<TextField
							id="text-input"
							label="Text"
							placeholder="Placeholder"
							fullWidth
							margin="normal"
							InputLabelProps={{
								shrink: true,
							}}
							variant="outlined"
						/>
						<TextField
							id="outlined-number"
							label="Number"
							placeholder="number"
							type="number"
							fullWidth
							margin="normal"
							InputLabelProps={{
								shrink: true,
							}}
							variant="outlined"
						/>
						<TextField
							id="outlined-email"
							label="Email"
							placeholder="email"
							type="email"
							fullWidth
							margin="normal"
							InputLabelProps={{
								shrink: true,
							}}
							variant="outlined"
						/>
            <TextField id="standard-select-currency-native"
                select label="Native select"
                value={currency} onChange={handleChange}
                SelectProps={{
                  native: true,
								}}
								variant="outlined"
                >
                {currencies.map((option) => (
                    <option key={option.value} value={option.value}>
                    {option.label}
                    </option>
                ))}
            </TextField>
						<div className={classes.button}>
							<Button variant="outlined" type="submit" color="primary">
								Submit
							</Button>
							<Button variant="outlined">
								Clear
							</Button>
						</div>
          </form>
        </Paper>
    );
}