import React, { useState, useMemo } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import AppHeader from "./component/AppHeader";
import MainTablePage from "./component/MainTablePage";
import MainTableDetailPage from "./component/MainTableDetailPage";
import FormCreatePage from "./component/FormCreatePage";
import { UserContext } from './context/UserContext';
import { AboutPage } from './component/AboutPage';

function App() {
  const [user, setUser] = useState(null);

  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  return (
    <BrowserRouter>
      <div>
        <AppHeader />
        <UserContext.Provider value={value}>
          <Switch>
            <Route path="/" component={MainTablePage} exact />
            <Route path="/form" component={FormCreatePage} exact />
            <Route path="/details/:id" component={MainTableDetailPage} exact />
            <Route path="/about" exact component={AboutPage}/>
          </Switch>
        </UserContext.Provider>
      </div>
    </BrowserRouter>
  );
}

export default App;
