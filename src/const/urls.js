const endpoint = "/api";

export const getMessages = endpoint + '/messages/filter';
export const getMessage = endpoint + '/messages/';